using System;
public class ConversionesDatos {
	public static void Main() {
	// COnvertir numero en texto:
	int edad = 27;
	string resultado;
	resultado = "" + edad;
	resultado = resultado + " era un numero y ahora es texto";
	Console.Write( resultado );
	double numDecimal = 6.4545f;
	resultado = resultado + ", el numDecimal = " +numDecimal;
	Console.WriteLine( resultado );
	
	// conversiones explicitas:
	float otroDecimal = (float) 1.23456789123;
	resultado =resultado + ", otroDecimal = " + otroDecimal;
	Console.WriteLine( resultado );
	
	// Conversiones complejas: de texto a numero
	int unEntero = Int32.Parse ("3434");
	resultado = resultado + ", unEntero = " + unEntero;
	Console.WriteLine ( resultado );
	
	string numA = "15", numB = "7";
	
	//hazque el programa calcule la suma y muestre el resultado
		
	int IntNumA = Int32.Parse (numA);
	int IntNumB = Int32.Parse (numB);
	int resultadoSuma = IntNumA + IntNumB;
	Console.WriteLine("El resultado es " + resultadoSuma );
	
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControladorJuego : MonoBehaviour
{
    public GameObject prefabBasura;
    public GameObject prefabsujetalatas;
    public GameObject prefabbotella;
    public int Vidas = 7;
    public int Puntos = 0;
    public GameObject TextoVidas;
    public GameObject TextoPuntos;

    private int puntosAnt;

    // Start is called before the first frame update
    void Start()
    {
        //mitad de probabilidades
        int numEnemigo = Random.Range(0, 3);
        if (numEnemigo == 0)
        { 
        GameObject.Instantiate(prefabBasura);
        }
        else if (numEnemigo == 1)
        {
        GameObject.Instantiate(prefabsujetalatas);
        }
        else if (numEnemigo == 2)
        {
            GameObject.Instantiate(prefabbotella);
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.TextoVidas.GetComponent<UnityEngine.UI.Text>().text = "Vidas: " + this.Vidas;
        this.TextoPuntos.GetComponent<UnityEngine.UI.Text>().text = "Puntos: " + this.Puntos;
       
        //this.puntosAnt = this.Puntos;
    }
    //Esto es un nuevo metodo (asscion, conjunto de instrucciones, funciones, procedimiento,mensaje...)
    public void CuandoCapturarEnemigo()
    {
        {
            this.Puntos = this.Puntos +10 ;
            this.InstanciarEnemigo();

        }
    }
    public void CuandoPerdemosEnemigo()
        {
            this.Vidas = this.Vidas - 1;
            this.InstanciarEnemigo();

        }
      
        public void InstanciarEnemigo()
        {
        int numEnemigo = Random.Range(0, 3);
        if (numEnemigo == 0)
        {
            GameObject.Instantiate(prefabBasura);
        }
        else if (numEnemigo == 1)
        {
            GameObject.Instantiate(prefabsujetalatas);
        }
        else if (numEnemigo == 2)
        {
            GameObject.Instantiate(prefabbotella);
        }


    }
    }


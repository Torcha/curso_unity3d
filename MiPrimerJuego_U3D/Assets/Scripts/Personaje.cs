﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public float velocidad = 40; // valor por defecto


    // Update is called once per frame
    void Update()
    {

        //cuando se pulsa izquierda

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Creamos el vector de movimiento, a partir del calculo que
            // inluye la velocidad (40), el vector hacua la izquierda (-1, 0, 0): (-40, 0, 0)
            // Para que se mueva -40 unid. por segundo en vez de por cada frame
            // Multiplicamos por el incremento del tiempo (aprox. 0.02 seg, 50 FPS) (0.8, 0, 0)
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
           
            // Una vez que se ha calculado, aplicamos el movimiento
            this.GetComponent<Transform>().Translate(vectorMov);

            // Si la posicion en el eje x ese menor que -17 (margen izqu)
            if (this.GetComponent<Transform>().position.x < -17 )
            {
                // Entonces recolocamos en el margen izq
                this.GetComponent<Transform>().position = new Vector3 (-17, 0, 0);
                Debug.Log("Choque a la izquierda");
            }

        }

        // cuando se pulsa a la derecha

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMov = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMov);

            // Si la posicion en el eje x es 20 (margen der)
            if (this.GetComponent<Transform>().position.x > 20)
            {
                // Entonces recolocamos en el margen der
                this.GetComponent<Transform>().position = new Vector3 (20, 0, 0);
                Debug.Log("Choque a la derecha");
            }
        }
        
    }

}

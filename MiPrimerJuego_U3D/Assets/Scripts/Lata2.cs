﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lata2 : MonoBehaviour
{
    public float velocidad;
    GameObject Jugador;   

    void Start()
    {
        float posInicioX = Random.Range(-17, 20);
        this.transform.position = new Vector3(posInicioX, 13, 1);
        Jugador = GameObject.Find("Jugador_Caballito");
    }
    void Update()
        
    {
        Vector3 movAbajo = velocidad
            * new Vector3(0, -1, 0)
            * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;

        if (this.GetComponent<Transform>().position.y < 0)
        {
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 1);

            if (this.transform.position.x >= Jugador.transform.position.x - 3.2f /2 
               && this.transform.position.x <= Jugador.transform.position.x + 3.2f /2)
            
            {
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoCapturarEnemigo();
                Destroy(this.gameObject);
            } else
            {
                GameObject.Find("Controlador_Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigo();
                Destroy(this.gameObject);
            }

        }
        
    }

}
